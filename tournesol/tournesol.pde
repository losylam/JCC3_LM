int n_circles;
int radius;
float angle;
float vit;

void setup() {
  size(1920, 1080);
  angle = 0;
  vit = 0.01;
  radius = 25;
  n_circles = 500;
}

void draw(){
  clear();

  n_circles = mouseY*2;
  vit = map(mouseX, 0, width, -0.05, 0.05);
  angle += vit;
  pushMatrix();
  translate(width/2, height/2);
  for (int i = 0; i < n_circles ; i++){
    float x = i*width*1.4/n_circles/2 * cos(radians(angle*i));// + i*width/2/n_circles;
    float y = i*width*1.4/n_circles/2 * sin(radians(angle*i));// + i*width/2/n_circles;
    colorMode(HSB);
    noStroke();
    fill(i*255/6%255, 255, 255);
    ellipse(x, y, radius, radius);
  }
  popMatrix();
}