#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

	gui.setup("tournesol");

	gui.add(n_colors.set("nombre de couleurs", 6, 1, 24 ));
	gui.add(n_graines.set("nombre de graines", 100, 1, 2000 ));
	gui.add(radius.set("rayon des graines", 10, 1, 50));
	gui.add(angle.set("angle", 0, 0, 360));
	gui.add(vitesse.set("vitesse angulaire", 0.01, -0.1, 0.1));
}

//--------------------------------------------------------------
void ofApp::update(){
	angle += ofMap(mouseX, 0, ofGetWidth(), -0.05, 0.05);
	n_graines = ofGetHeight() - mouseY;
	//gui.update();
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofSetBackgroundColor(0);
	ofDrawBitmapString("Hello World !", 20, 20);
	ofPushMatrix();
	ofTranslate(ofGetWidth()/2, ofGetHeight()/2);
	for (int i = 0; i < n_graines ; i++){
		int x = i*ofGetWidth()*1.2/2/n_graines * cos(ofDegToRad(angle*i));
		int y = i*ofGetWidth()*1.2/2/n_graines * sin(ofDegToRad(angle*i));
		ofColor color = ofColor().fromHsb((i*255/n_colors)%255, 255, 255);
		ofSetColor(color);
		ofDrawCircle(x, y, radius);
	}
	ofPopMatrix();
	gui.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	switch(key){
	case 'f':
		ofToggleFullscreen();
		break;
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
