PVector pos;
PVector vit;

int n_balles = 1000;

Balle[] balles;

class Balle {
  PVector pos;
  PVector vit;
  int radius;
  color col;
  Balle(){
    pos = new PVector(random(0, width), random(0, height));
    vit = new PVector(random(-5, 5), random(-5, 5));
    radius = int(random(10, 30));
    colorMode(HSB);
    col = color(random(100, 150), random(200, 255), random(200, 255));
  }
  void update(){
    pos = pos.add(vit);
    if(pos.x > width){
      pos.x = 0; 
    }else if(pos.x < 0){
      pos.x  = width;
    }
    if(pos.y > height){
      pos.y = 0; 
    }else if(pos.y < 0){
      pos.y  = height;
    }
  }
  void draw(){
    update();
    fill(col);
    ellipse(pos.x, pos.y, radius, radius);
  }
}

void setup(){
    size(1440, 720);
    balles = new Balle[n_balles];
    for(int i = 0; i< n_balles; i++){
      balles[i] = new Balle();
    }
}

void draw(){
  clear();
    for(int i = 0; i< n_balles; i++){
      balles[i].draw();
    }
}
