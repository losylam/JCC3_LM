#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
  ofSetBackgroundAuto(False);
  pos = ofPoint(ofGetWidth()/2, ofGetHeight()/2); 
  vit = ofPoint(ofRandom(10), ofRandom(10));

  for (int i = 0; i < NPARTS; i++){
    part[i] = Particule();
  }
}

//--------------------------------------------------------------
void ofApp::update(){
  for (int i = 0; i < NPARTS; i++){
    part[i].update_bounce();
  }
}

//--------------------------------------------------------------
void ofApp::draw(){
  //ofBackgroundGradient(ofColor(0, 0, 0), ofColor(30, 30, 30));
  for (int i = 0; i < NPARTS; i++){  
    part[i].draw();
  }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
  switch(key){
  case 'f':
    ofToggleFullscreen();
    ofBackgroundGradient(ofColor(0, 0, 0), ofColor(30, 30, 30));
    break;
  }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){
  float fact = float(y) / float(ofGetHeight());
  for (int i = 0; i < NPARTS; i++){
    part[i].set_vel(fact);
  } 
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
  int min_col = ofRandom(200);
  int max_col = ofRandom(min_col, 255);
  for (int i = 0; i < NPARTS; i++){
    part[i].arcenciel(min_col, max_col);
  }
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
