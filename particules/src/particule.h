#ifndef PARTICULE_H
#define PARTICULE_H

#include "ofMain.h"

class Particule {

 public:
  Particule();

  void update();
  void update_bounce();
  void draw();

  void arcenciel(int min_col, int max_col);
  void set_vel(float vel);

  ofPoint pos;
  ofPoint vit;
  ofColor color;
  int radius;
  float vel;
  
};
#endif
