#include "particule.h"

Particule::Particule(){
  pos = ofPoint(ofRandom(ofGetWidth()), 
		ofRandom(ofGetHeight()));
  vit = ofPoint(ofRandom(-10, 10),
		ofRandom(-10, 10));
  color = color.fromHsb(ofRandom(200, 255), ofRandom(100, 200), 255);
  radius = ofRandom(20);
  vel = 1;
}

void Particule::update(){
  pos += vit*vel;
   
  if (pos.x > ofGetWidth()){
    pos.x = 0;
  }else if (pos.x < 0){
    pos.x = ofGetWidth();
  }

  if (pos.y > ofGetHeight()){
    pos.y = 0;
  }else if (pos.y < 0){
    pos.y = ofGetHeight();
  }
}

void Particule::update_bounce(){
  pos += vit*vel;
   
  if (pos.x > ofGetWidth() or pos.x < 0){
    vit.x *= -1;
  }

  if (pos.y > ofGetHeight() or pos.y < 0){
    vit.y *= -1;
  }
}

void Particule::arcenciel(int min_col, int max_col){
  color = color.fromHsb(ofMap(pos.x, 0, ofGetWidth(), min_col, max_col), 155, 255); 
}

void Particule::set_vel(float vel_in){
  vel = vel_in;
}

void Particule::draw(){
  ofPushStyle();
  ofSetColor(color);
  ofDrawCircle(pos, radius);
  ofPopStyle();
}
